Though not more popular than other crates yet, `tbot` is already used to build
bots and programs for the real world. Here is a list of projects that alredy use
`tbot`, including some dogfooding ones.

If you want to add your bot to this list, open a merge request against
[this wiki's repository][wiki-git]!

[wiki-git]: https://gitlab.com/SnejUgal/tbot-wiki

## `noce` — *n*otify *o*n *c*ommand *e*xit

- Source code: [GitLab](https://gitlab.com/AlexStrNik/noce)

A small tool written in pure Rust and `tbot`, which sends you a notification
via your bot when a command finishes its execution.

### Installation

```bash
cargo install noce
```

### Usage

```bash
NOCE_BOT_TOKEN=… NOCE_CHAT_ID=… noce --stdout --time -- echo "Hello, noce!"
```

## @invertatthemebot

- Telegram: [@invertatthemebot](https://t.me/invertatthemebot)
- Source code: [GitLab](https://gitlab.com/SnejUgal/invertatthemebot)

A bot that inverts Telegram themes in different ways.

## @atthemeimagebot

- Telegram: [@atthemeimagebot](https://t.me/atthemeimagebot)
- Source code: [GitLab](https://gitlab.com/SnejUgal/atthemeimagebot)

A bot that can extract wallpapers from Android Telegram themes and set one.

## @randomatthemebot

- Telegram: [@randomatthemebot](https://t.me/randomatthemebot)
- Source code: [GitLab](https://gitlab.com/SnejUgal/randomatthemebot)

A bot that generates random Android Telegram themes.

## vk-to-telegram-bot

- Source code: [GitLab](https://gitlab.com/SnejUgal/vk-to-telegram-bot)

A bot that notifies you in Telegram about new posts in VK groups.
