We're going to write a bot that can evaluate math expressions and send the
result in chats and inlinely. You'll need to:

- [Be friends with Rust][book];
- Have basic knowledge of [asynchromous programming in Rust][async].

[book]: https://doc.rust-lang.org/book/index.html
[async]: https://blog.rust-lang.org/2019/11/07/Async-await-stable.html

## Installing `tbot`

Create a new crate:

```bash
cargo new tbot_example
cd tbot_example
```

In your `Cargo.toml`, add this:

```toml
[dependencies]
tbot = "0.3"
tokio = { version = "0.2", features = ["macros", "sync"] }
meval = "0.2"
```

You'll use [`meval`] to evaluate math expressions, `tbot` to make the bot
and `tokio` to start the runtime (you'll need the `sync` feature to use
the asynchronous version of `Mutex` later).

[`meval`]: https://docs.rs/meval/0.2

## Getting updates

First of all, you'll need your bot's token. If you don't have one already,
create one with [BotFather] and it will give you the token. We'll assume you
set it in the environment as `BOT_TOKEN`.

Once you're done with `Cargo.toml` and the token, open `src/main.rs`. First, in
`main`, we will create our bot with the token in the `BOT_TOKEN` environment
variable:

```rust
let mut bot = tbot::from_env!("BOT_TOKEN").event_loop();
```

> The [`from_env!`] macro allows extracting the variable _at compile time_. You
> can replace it with [`Bot::from_env`] if you want to extract it _at runtime_.

Using the [`from_env!`] macro, we create a [`Bot`]. However, to configure
handlers and run them, we need to create an [`EventLoop`] using
[`Bot::event_loop`].

Our bot is going to listen to text messages. So, let's add a handler:

```rust
bot.text(|_context| {
    async move {
        println!("Someone sent me a message!");
    }
});
```

> Handlers are required to return a [`Future`] so that you don't have to spawn
> futures yourself — `tbot` does that for you. Here, our handler is a closure
> whose body is wrapped in an `async` block, and we're going to stick to this
> style for the rest of the tutorial.

That's why we need `mut` on `bot`: [`EventLoop::text`] pushes the handler in
`bot`'s internal `Vec`. That's a trade-off for a really convenient update
subscription mechanism. We'll learn about `context` a bit later.

Though we added a handler, one thing is missing: we need to actually listen
to updates (messages are a subset of updates). This can be done with webhooks
or polling. Polling is simpler, so let's use it:

```rust
bot.polling().start().await.unwrap();
```

> The [`Future`] returned by [`Polling::start`] resolves to a `Result`, but
> it can only return an error if the error occured during initialization — after
> that, the event loop runs for ever.

Now, run `cargo run` and try to send your bot a message. It won't reply you yet,
but you'll see a message in your terminal that it received a message!

[botfather]: https://t.me/BotFather
[`from_env!`]: https://docs.rs/tbot/0.3/tbot/macro.from_env.html
[`bot::from_env`]: https://docs.rs/tbot/0.3/tbot/struct.Bot.html#method.from_env
[`bot`]: https://docs.rs/tbot/0.3/tbot/struct.Bot.html
[`eventloop`]: https://docs.rs/tbot/0.3/tbot/event_loop/struct.EventLoop.html
[`bot::event_loop`]: https://docs.rs/tbot/0.3/tbot/struct.Bot.html#method.event_loop
[`eventloop::text`]: https://docs.rs/tbot/0.3/tbot/event_loop/struct.EventLoop.html#method.text
[`polling::start`]: https://docs.rs/tbot/0.3/tbot/event_loop/struct.Polling.html#method.start

## Processing updates and sending a reply

We can already receive updates, but how do we _process_ them? It's easy if you
try: remember `context`? That's where updates are coming to. To get the text of
the message, we need to use `context.text.value`. Let's print it:

```rust
bot.text(|context| {
    async move {
        println!("Someone sent me this: {}", context.text.value);
    }
});
```

> [`context.text`] is of the [`types::Text`] type which also has the `entities`
> field for entities in the message.

Now try it out. You will see your messages in the terminal.

Let's send a reply. First, we'll use
[`context.send_message_in_reply`]:

```rust
// ...
let message = format!("You sent me {}", context.text.value);
context.send_message_in_reply(&message).call().await.unwrap();
```

[`context.send_message_in_reply`] will construct a method with the given text
and a reply to that message. Because methods have optional fields, they are
set with chained methods. Once you finish building the method, you must `call`
and `.await` it. The returned [`Future`] resovles to a `Result` — whether the
call sucessed or failed. Here, we simply unwrap the `Result`, but in production,
you should handle errors properly.

Now you can run your bot again, and it will reply you.

But we actually want a math bot, not an echo one! Let's do it now.

[`context.text`]: https://docs.rs/tbot/0.3/tbot/contexts/struct.Text.html#structfield.text
[`types::text`]: https://docs.rs/tbot/0.3/tbot/types/message/text/struct.Text.html
[`context.send_message_in_reply`]: https://docs.rs/tbot/0.3/tbot/contexts/traits/trait.ChatMethods.html#method.send_message_in_reply
[`future`]: https://doc.rust-lang.org/std/future/trait.Future.html

## Doing math

We're going to use [`meval`] so we won't need to do math ourselves.

```rust
// We're going to import another struct with the name `Text` later
use tbot::types::parameters::Text as ParseMode;

bot.text(|context| {
    async move {
        let message = if let Ok(answer) = meval::eval_str(&context.text.value) {
            format!("= `{}`", result)
        } else {
            "Whops, I couldn't evaluate your expression :(".into()
        };

        context
            .send_message_in_reply(ParseMode::markdown(&message))
            .call()
            .await
            .unwrap();
    }
});
```

We wrap the result in Markdown's backticks, so it may be easier to copy
the result in some clients (e.g. on Android). We need to tell Telegram to parse
this message as Markdown, so, instead of passing a bare string, we wrap the
message in [`Text`][parameters::text], calling the [`Text::markdown`] method.

Now the bot will evaluate experessions it receives. Try it out!

[parameters::text]: https://docs.rs/tbot/0.3/tbot/types/parameters/struct.Text.html
[`text::markdown`]: https://docs.rs/tbot/0.3/tbot/types/parameters/struct.Text.html#method.markdown

## Inline mode

Now we're going to implement the inline mode. It isn't hard to do.

First, ensure that your bot can accept inline updates. Go to [BotFather], choose
your bot, click `Bot Settings` → `Inline Mode`. It's off by default, so turn it
on if you haven't done it yet.

Next, we'll add another handler:

```rust
bot.inline(|_context| async { });
```

Note that for inline handlers, the `context` is completely different, but we'll
get through that. Instead of `context.text.value`, we need to use
[`context.query`]. Instead of [`context.send_message_in_reply`], we need to use
[`context.answer`]. Through replacing the first one is easy, the second isn't.

```rust
use std::sync::Arc;
use tbot::types::{
    inline_query::{self, result::Article},
    input_message_content::Text,
};
use tokio::sync::Mutex;
// ...
let id: Arc<Mutex<u32>> = Arc::new(Mutex::new(0));
bot.inline(move |context| {
    let id = Arc::clone(&id);
    async move {
        let (title, message) = match meval::eval_str(&context.query) {
            Ok(result) => (
                result.to_string(),
                format!("`{} = {}`", context.query, result),
            ),
            Err(_) => (
                "Whops...".into(),
                "I couldn't evaluate your expression :(".into(),
            ),
        };

        let id = {
            let id = id.lock().await;
            id += 1;
            id.to_string()
        };

        let content = Text::new(ParseMode::markdown(&message));
        let article = Article::new(&title, content).description(&message);
        let result = inline_query::Result::new(&id, article);
        context.answer(&[result]).call().await.unwrap();
    }
});
```

First of all, we evaluate the requested expression. Then, we need to generate
an ID, so we previously declared a [`Mutex`] wrapped in an [`Arc`], which we
then move to the handler, and when we actually need to generate an ID, we lock
the mutex, increment the ID and stringify it.

Then we need to generate the result that we're going to send to Telegram.
It's possible to show several results, but we only need only one — with the
calculation result. We construct it step-by-step:

1. `content` — this is what the user will send when they choose a result.
   [`InputMessageContent`] is divided into four variants, and we construct the
   [`Text`] variant.
2. `article` — this is one kind of [`inline_query::Result`]. It requires
   a title and any kind of [`InputMessageContent`]. In addition, we set
   its [`description`] to what we're going to send.
3. `result` — the result itself. Construction requires an ID and any kind
   of [`inline_query::Result`].

Finally, we call [`context.answer`] with a slice of one item, the `result`.
And voila! Your bot now can work inlinely. If you need it, here's the complete
[code] (and with better error handling, too).

[`context.query`]: https://docs.rs/tbot/0.3/tbot/contexts/struct.Inline.html#structfield.query
[`context.answer`]: https://docs.rs/tbot/0.3/tbot/contexts/struct.Inline.html#method.answer
[`mutex`]: https://docs.rs/tokio/0.2/tokio/sync/struct.Mutex.html
[`arc`]: https://doc.rust-lang.org/std/sync/struct.Arc.html
[`inputmessagecontent`]: https://docs.rs/tbot/0.3/tbot/types/input_message_content/enum.InputMessageContent.html
[`text`]: https://docs.rs/tbot/0.3/tbot/types/input_message_content/struct.Text.html
[`inline_query::result`]: https://docs.rs/tbot/0.3/tbot/types/inline_query/result/struct.Result.html
[`description`]: https://docs.rs/tbot/0.3/tbot/types/inline_query/result/article/struct.Article.html#method.description
[code]: https://gitlab.com/SnejUgal/tbot/blob/master/examples/tutorial.rs

## What's next?

Now you're familiar with `tbot`, and you can start writing your own bots. You
may want to check our [`How-to` guides][howto] if you need or refer to our
[documentation] to look up how to use several methods or construct some types.
If you get stuck, feel free to ask your question in
[our group on Telegram][group].

[howto]: ./How-to
[documentation]: https://docs.rs/tbot
[group]: https://t.me/tbot_group
